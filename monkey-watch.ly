\version "2.24.3"
\language "english"

setup = {
  \key gf \major
  \time 4/4
  \tempo 4 = 160
  \clef tenor
  \set Staff.midiInstrument = #"acoustic grand"
  \transposition c
}

theme = \relative c'' {
  \setup
  \clef "tenor^8"
  R1
  r8 df, f af df4 df, |
  % main theme
  r4 df'4 df df8 ef8~ | ef8 ef4 ef8 df4 cf4 | df4. bf4 r8 r4 |
  R1
  % second main theme
  r4 df4 df bf8 cf8~ | cf8 cf4 cf8 cf8 df8 ef8 cf8 | df4. gf4 r8 r4 |
  R1
  % main theme again  
  r4 df4 df df8 ef8~ | ef8 ef4 ef8 df4 cf4 | df4. bf4 r8 r4 |
  R1
  % funky zone
  r4 bf4 bf4 gf8 af8~ af8 af4 af8 af8 bf8 cf8 ef8 df2 r2
  \mark \default
  r4 e4 ef4 df4
  % jazz theme 1
  df4 cf4 bf8 cf4 df8~ | df4 ef8 gf,4 ef8 gf af |
  bf4 af gf8 af4 af8~ | af4. bf4. r4 |
  % jazz theme 2
  r4 bf4 bf8 bf4 af8~  af4. df4 cf8 bf af gf4
  gf af8 gf af gf8~ gf2 r2 |
  % jazz theme 1 again
  df'4 cf4 bf8 cf4 df8~ | df4 ef8 gf,4 ef8 gf af |
  bf4 af gf8 af4 af8~ | af4. bf4. r4 |
  % funky link
  r4 gf4 gf8 gf4 df'8~ | df8 cf8 bf8 af8 r8 gf 4 gf8~ |
  \mark \default
  gf1 | r4 gf gf gf | gf1 |
  r4 df4 cf8 bf4. |
  % bridge
  gf'8 gf gf gf gf r e r | gf8 gf gf gf gf8 a af a |
  gf8 gf gf gf gf r e r | gf8 gf gf gf gf8 g af a |
  b8 b b b b r a r | b8 b b b b8 d df d |
  b8 b b b b r a r | b8 bf a af8 r8 g r g |
  \mark \default
  % main theme
  r4 df'4 df df8 ef8~ | ef8 ef4 ef8 df4 cf4 | df4. bf4 r8 r4 |
  R1
  % second main theme
  r4 df4 df bf8 cf8~ | cf8 cf4 cf8 cf8 df8 ef8 cf8 | df4. gf4 r8 r4 |
  R1
  % main theme again  
  r4 df4 df df8 ef8~ | ef8 ef4 ef8 df4 cf4 | df4. bf4 r8 r4 |
  R1
  \mark \default
  r4 bf4 bf gf8 af8~ | af8 af4 af8 af gf f4 | gf4 r4 r2 | R1 |
  
  r4 bf4 bf gf8 af8~ | af8 af4 af8 af gf f4 | gf1~ | gf1~ | gf1~ | gf1
  \bar "|."
}

harmony = \relative c'' {
  \setup
  \clef "tenor^8"
  R1*6
  % join the theme variation
  r4 bf4 bf gf8 af8~ | af8 af4 af8 af bf cf af | bf4. df4 r8 r4 |
  R1
  % main theme harmony
  r4 bf4 bf bf8 cf8~ | cf8 cf4 cf8 bf4 af | bf4. gf4 r8 r4 |
  R1
  R1 | r4 r f8 gf af cf8 | bf2 r2 |
  \mark \default
  r4 df4 cf bf |
  % jazzy main bit
  bf af gf8 af4 bf8~ | bf4 cf8 gf4 bf,8 ef8 f8 |
  gf 4 f ef8 f4 f8~ | f4. gf4. r4 |
  r4 gf gf8 gf4 f8~ | f4. bf4 af8 gf f | ef4 ef f8 ef f df~ | df2 r2 | 
  % repetition of earlier jazzy main bit
  bf'4 af gf8 af4 bf8~ | bf4 cf8 gf4 bf,8 ef8 f8 |
  gf 4 f ef8 f4 f8~ | f4. gf4. r4 |
  r4 ef ef8 ef4 bf'8~ | bf8 af gf f r ef4 ef8~ |
  ef1 | r4 ef ef ef | d1 |
  r4 df4 cf8 bf4. |
  % bridge
  bf8 bf bf bf bf r af r | bf8 bf bf bf bf df c df |
  bf8 bf bf bf bf r af r | bf8 bf bf bf bf cf? c df |
  ef8 ef ef ef ef r df r | ef8 ef ef ef ef gf f gf |
  ef8 ef ef ef ef r df r | ef8 d df c r cf r cf |
  \mark \default
  R1*4
  % funky new theme variation
  r4 bf'4 bf gf8 ef'8~ | ef8 ef4 ef8 ef f gf ef | f4. bf4 r8 r4 |
  R1
  % main theme harmony
  r4 bf,4 bf bf8 cf8~ | cf8 cf4 cf8 bf4 af | bf4. gf4 r8 r4 |
  R1
}

countertheme = \relative c'' {
  \setup
  \clef "tenor^8"
  R1*2
  % main theme start
  R1*2
  r2 r8
  gf8 af8 bf8 cf8 bf8 af8 gf8 bf4 af4 gf4
  r4 r2
  R1
  r2 r8
  gf8 af8 bf8 cf8 bf8 af8 gf8 r8 df'8 r8 df8
  R1*2
  r2 r8
  gf, 8 af8 bf8 cf8 bf8 af8 gf8 bf4 af4 gf4
  r4 r2
  R1
  r4 r8
  df'4 <cf ef>8 <bf df>8 <af cf>8
  \mark \default
  <g bf>4 
  r4 r2
  R1*2 r2 r4.
  df'8~ df8 cf4 bf4 af4 gf8~ gf4 r4 r2 
  R1 r2 r4.
  df'8~ | df8 bf8 cf df r e4. | ef4.
  r8 r2 | R1
  r2 r4.
  df8~ df8 cf4 bf4 g4 ef8~ ef2. r4 | R1
  \mark \default
  ef8 ef f gf af bf cf df | ef1 |
  d,8 ef f gf af bf cf df | d1 |
  % bridge
  R1*4
  gf,4 gf'2 gf,4 | gf'2 gf,4 gf'4~ | gf4 gf, gf'2 |
  R1
  \mark \default
  % main theme again, kinda
  R1*2
  r2 r8
  gf,8 af8 bf8 cf8 bf8 af8 gf8 bf4 af4 gf4
  r4 r2
  R1
  r2 r8
  gf8 af8 bf8 cf8 bf8 af8 gf8 r8 df8 r8 df8
  R1*2
  r2 r8
  gf8 af8 bf8 cf8 bf8 af8 gf8 bf4 af4 gf4 r4 r2 |
  R1
  ef'4 df8 bf4 bf8 cf df | ef cf gf'4 f8 ef df4 |
  R1*2
  % snazzy solo time!
  r8 gf,, bf df gf df gf af | bf gf bf df gf2~ | gf1~ | gf1
}

highbacking = \relative c'' {
  \setup
  \clef "tenor^8"
  R1*2
  % main theme
  r4 gf4 gf8 f ef cf | gf'2 f2 | f4. gf4 r8 r4 | <ef gf>2 f2 |
  r4 gf4 gf8 f ef cf | ef2 f2 | f4. gf4 r8 r4 | <ef gf>2 f2 |
  r4 gf4 gf8 f ef cf | gf'2 f2 | <f af>4. gf4 r8 r4 | <ef gf>2 f2 |
  gf4. gf8 gf ef ef4 | <ef gf>2 f2 | f4. f8~ f2 |
  \mark \default
  <ef bf'>2 <ef cf'>2 | gf2 gf8 ef4 <f bf>8~ | <f bf>4. gf8~ gf2 |
  ef2. r8 f8~ | f2 e2 | ef? 2 r2 |
  R1
  r2 r4. gf8~ | gf4. e8 r8 e4. | gf2 gf8 ef4 <f bf>8~ | <f bf>4. gf8~ gf2 |
  R1*3
  r2 r8 bf8 ef f |
  gf4 gf gf8 gf r gf8~ | gf4 gf gf8 gf r gf8~ |
  gf4 gf gf8 gf r gf8~ | gf4 gf gf8 gf4. |
  % bridge
  R1*8
  \mark \default
  % main theme again
  r4 gf,4 gf8 f ef cf | gf'2 f2 | f4. gf4 r8 r4 | <ef gf>2 f2 |
  r4 gf4 gf8 f ef cf | ef2 f2 | f4. gf4 r8 r4 | <ef gf>2 f2 |
  r4 gf4 gf8 f ef cf | gf'2 f2 | <f af>4. gf4 r8 r4 | <ef gf>2 f2 |
  \mark \default
  r4 gf4 gf8 f ef cf | ef2 f2 | gf2 r2 | R1
  r4 gf4 gf8 f ef cf | ef2 f2 | gf1~ | gf1~ | gf1~ | gf1
}

backing = \relative c' {
  \setup
  \clef bass
  R1
  r8 df,8 f8 af8 df4 df,4
  % backing for first bit
  gf4 r8 gf8 ef4 r8 bf8 cf4 r8 cf8 df4 df4 bf4 r8
  bf8 ef4 r8 bf8 cf4 r8 cf8 df4 df8 f8
  % second backing
  gf4 r8 gf8 ef4 r8 bf8 af4 r8 af8 df4 cf4 bf4 r8 
  bf8 ef4 r8 bf8 cf4 r8 cf8 df4 df8 f8 
  % backing for first bit again
  gf4 r8 gf8 ef4 r8 bf8 cf4 r8 cf8 df4 df4 bf4 r8
  bf8 ef4 r8 bf8 cf4 r8 cf8 df4 df8 f8
  % second backing again
  gf4 r8 gf8 ef4 r8 bf8 af4 r8 af8 df4 cf4 bf4 r8
  % transition
  bf8 bf4 r8 bf8 
  \mark \default
  ef4 r8 ef8 ef8 bf8 ef8 df8
  cf4 r8 cf8 cf4 r8 bf8 | bf8 bf8 r8 gf8 bf8 gf8 df'8 gf,8 | 
  af4 r8 af8 df4 r8 gf,8 | gf8 gf8 r8 gf8 gf8 df8 af'8 f8 |
  cf'4 r8 cf8 cf gf cf bf | bf8 bf r bf bf f bf af |
  af4 r8 af8 df8 e, f gf~ | gf4 r8 df'4 gf,4 bf8 |   
  % slight repeat
  cf4 r8 cf8 cf4 r8 bf8 | bf8 bf8 r8 gf8 bf8 gf8 df'8 gf,8 |
  af4 r8 af8 df4 r8 gf,8 | gf gf r8 a8 a e a e |
  af4 r8 af8 af af r8 bf | bf8 af gf ef r bf'4 gf8 |
  cf4 r8 cf8 cf cf r cf | cf4 r8 cf cf cf r cf | cf4 r8 cf cf cf r cf |
  cf4 r8 cf8 df f4. |
  % bridge
  gf8 gf gf gf gf r e r | gf8 gf gf gf gf8 a af a |
  gf8 gf gf gf gf r e r | gf8 gf gf gf gf8 g af a |
  b8 b b b b r a r | b8 b b b b8 d df d |
  b8 b b b b r a r | b8 bf a af8 r8 g r g |
  \mark \default
  % backing for first bit
  gf4 r8 gf8 ef4 r8 bf8 cf4 r8 cf8 df4 df4 bf4 r8
  bf8 ef4 r8 bf8 cf4 r8 cf8 df4 df8 f8
  % second backing
  gf4 r8 gf8 ef4 r8 bf8 af4 r8 af8 df4 cf4 bf4 r8 
  bf8 ef4 r8 bf8 cf4 r8 cf8 df4 df8 f8 
  % backing for first bit again
  gf4 r8 gf8 ef4 r8 bf8 cf4 r8 cf8 df4 df4 bf4 r8
  bf8 ef4 r8 bf8 cf4 r8 cf8 df4 df8 f8
  \mark \default
  gf4 r8 gf8 ef4 r8 bf8 | af4 r8 af8 df4 df4 | ef4 r8 ef8 bf4 r8 bf8 |
  b4 r8 b8 df4 f4 | gf2 ef2 | af,2 df2 | gf1~ | gf1~ | gf1~ | gf1
}

\layout {
  \context {
    \Staff
    \RemoveEmptyStaves
  }
}

\book {
  \header {
    title = "Monkey Watch"
    subtitle = "from Rhythm Heaven Fever"
    composer = "Nintendo"
  }
  \score {
    <<
     \new GrandStaff <<
      \new Staff \theme
      \new Staff \harmony
      \new Staff \countertheme
      \new Staff \highbacking
      \new Staff \backing
     >>
    >>
    \layout {}
    \midi {}
  }
}