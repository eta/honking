% RUINS, from the UNDERTALE OST
% This file was autoconverted -- still a WIP
% Lily was here -- automatically converted by /usr/bin/midi2ly from /home/eta/Documents/MuseScore3/Scores/Ruins.mid
\version "2.14.0"

\layout {
  \context {
    \Voice
    \remove "Note_heads_engraver"
    \consists "Completion_heads_engraver"
    \remove "Rest_engraver"
    \consists "Completion_rest_engraver"
  }
}

setup = {
  \key ges \major
  \time 3/4
  \tempo 4 = 138
  \clef bass
  \set Staff.midiInstrument = #"trombone"
  \transposition c
}

leitmotif = {
  aes8 ees' ces4 bes8 ces des
}

theme = {
  ees4. aes4 ces8 aes r8 
  bes ces des bes 
  ces4. bes4 aes16 ges f8 r8 
  ces' des ees f 
  ces4. bes4 ees,8 
  aes4 ges f
  ges8 f ees des4
  aes'8 ees4 r8 
}

introA = {
  \leitmotif r8
  r4. ges,8 \leitmotif r8

  f ges f des

  \clef "tenor^8"
  
  \theme

  bes' ces bes des4 r2.

  f,8 ges f ges f4. aes ees
  bes8 ces bes des4 r8*7

  f8 ges f aes4 r4*5

  \clef tenor
}

introB = {
  r1*3 \leitmotif r2
  ges,8 \leitmotif r8*5
  \leitmotif r2
  ges,8 \leitmotif r4
  aes8 bes aes \leitmotif r8 
  aes bes ces bes \leitmotif r8
  aes bes ces bes \leitmotif r8*5
  \relative c' { \leitmotif } r8*5
}

lineA = \relative c' {
  \setup
  \clef tenor
  \set Staff.instrumentName = "Tbn. 1"

  \introA |

  % chorus

  \repeat unfold 6 {
    ges8 r8 ges ges r8 ges | aes4 r8 ges f ges |
  }

  ges r8 ges r4 ges8 | aes4 r2 |
  
  \leitmotif r8*5 |

  % funky section

  aes4 r8 aes' f ees |

  des r4 des8 r8 des |
  bes r8 des r8 bes r8 |

  g r4 g8 r4 |
  g a e |

  ces'8 r4 ces8 r8 ces | 
  aes r8 ces r8 aes r8 |

  r8*72

  ees8 bes' ges4 f8 ges | aes r8*5
  ees8 bes' ges4 f8 ges | aes 
}

lineB = \relative c {
  \setup
  \set Staff.instrumentName = "Tbn. 2"

  \introB |
  
  ees2. des4 r8 
  ees4 des8 
  ees2 des8 
  ees2 des8 
  ees 
  des 
  ees2. des4 r8 
  ees4 des8 
  ees2 des8 
  ees2 des8 
  ees 
  des 
  ees2. des4 des8 
  ees4 r8 
  des 
  ees2 r8 
  des4 r8 
  des 
  ees4 des8 
  ees2 r8 
  des4 r2 des4 r2 des4 r2 des4 r2 bes8 
  bes4 r8 
  bes 
  bes 
  bes 
  bes 
  r2 a8 
  a4 r8 
  a 
  a 
  a 
  a 
  r4. a8 
  aes 
  aes4 r8 
  aes 
  aes 
  aes 
  aes 
  
}

lineC = \relative c {
  \setup
  \clef bass
  \set Staff.instrumentName = "Tbn. 3"

  r1*15 aes'8 
  r8 
  ces 
  bes4 ces8 
  des4 r8 
  aes 
  bes 
  ges 
  aes 
  r8 
  ces 
  bes4 ees8 
  f4 f8 
  des 
  bes 
  ges 
  aes 
  r8 
  ces 
  bes4 ces8 
  des4 r8 
  aes 
  bes 
  ges 
  aes 
  r8 
  ces 
  bes4 ees8 
  f4 r2 aes,8 
  r8 
  ces 
  bes4 ces8 
  des4 r2 aes8 
  r8 
  ces 
  bes4 ces8 
  des4 r2 aes8 
  r8 
  ces 
  r4 ces8 
  des4 r1*2 des4 r4 aes,8 
  a 
  bes 
  f' 
  des4 bes8 
  f' 
  bes, 
  f' 
  bes4 r4 a,8 
  e' 
  des4 a8 
  e' 
  a, 
  e' 
  g4 ges8 
  e 
  aes, 
  ees' 
  ces4 aes8 
  ees' 
  aes, 
  ees' 
  aes4 ees bes8 
  des 
  f4 des des ees f 
}

lineD = \relative c {
  \setup
  \clef tenor
  \set Staff.instrumentName = "Tbn. 4"

  r1*15 ees'4. aes4 ces8 
  aes 
  r8 
  bes 
  ces 
  des 
  bes 
  ces4. bes4 aes16 ges 
  f8 
  r8 
  ces' 
  des 
  ees 
  f 
  ces4. bes4 ees,8 
  aes4 ges f ges8 
  f 
  ees 
  des4 aes'8 
  ees4 r8 
  bes 
  ces 
  bes 
  des4 r2. f8 
  ges 
  f 
  ges 
  f4. aes ees bes8 
  ces 
  bes 
  des4 r8*7 f8 
  ges 
  f 
  aes4 r4*5 ees4. bes8 
  aes 
  bes 
  bes4 r4 bes bes bes8 
  ees, 
  f 
  aes 
  a4 r4 a a r2 aes4 r4 aes aes r2 ges4 r4 ges ges r2 aes4 r4 aes 
  aes 
}


lineE = \relative c {
  \setup
  \clef tenor
  \set Staff.instrumentName = "Tbn. 5"

  r8*219 bes'8 
  ces 
  bes 
  des8*9 bes8 
  ces 
  bes 
  des2. g4. ges8 
  e 
  g 
  ces,1 bes4 ces bes4. ces16 bes 
  aes8 
  bes 
  f'2 des4 ees4*5 f16 ees 
  des8 
  ees2 bes4 ges aes bes des4*5 ees16 des 
  ces8 
  des2. c des c f1. 
}

\score {
  <<
    \new GrandStaff <<
      \new Staff \lineA
      \new Staff \lineB
      \new Staff \lineC
      \new Staff \lineD
      \new Staff \lineE
    >>
  >>
  \layout {}
  \midi {}
}
