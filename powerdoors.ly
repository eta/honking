% POWER DOORS
% Operation by Driver Only

% General Appendix Instructions
% 7th October 1989

% [source: https://www.youtube.com/watch?v=pUZ_NgMaI9g]

\version "2.14.0"

setup = {
  \time 4/4
  \set Staff.midiInstrument = #"trombone"
  \tempo 4 = 163
  \transposition c
}

\layout {
\context {
    \Voice
    \remove "Note_heads_engraver"
    \consists "Completion_heads_engraver"
    \remove "Rest_engraver"
    \consists "Completion_rest_engraver"
  }
}

percussion = \drummode {
  \time 4/4
  \set Staff.instrumentName = "Drums"
  \tempo 4 = 163
  \repeat unfold 3 {
    bd4 hh8 bd4 hc8 hc4
    bd4 hh bd4 hh 
  }
  bd4. hh4 bd bd
  tomh tomh16 tomh toml8 tomh8 tomml toml
  
  bd4 r hh r
  bd bd hh r
  bd4 r hh r8 bd
  r tomml r tomml r8
  tomml16 tomml toml8 bd8~ bd4
  
  hh4 r8 hc8 hc \noBeam bd8~ bd
  
  bd8~ bd4 hh hh hh8
  
  tomml8 toml bd4~ bd8 hh4
  
  r8 hc hc
  
  bd4 r
  
  hho8 r4 tomh16 tomml <toml hho>4 r8
  
  hho4
  
  tomh16 tomml toml8
  
  tomh16 tomml tomh tomh tomml tomml toml8
  
  tomh16 toml bd8~ bd4 hh
  hh8 hh hh r8
  r1
}

bassline = \relative c, {
  \clef bass
  \set Staff.instrumentName = "Tbn. 2"
  \setup
  a'4 r8 a8 r8 a r8 d8 r8 d r8 d e r8 gis, 
  r8 a r4 a8 r8 a r8 
  d r8 d r8 d e r8 gis, 
  r8 a r4 a8 r8 a r8 
  d r8 d r8 d e r8 gis, 
  r8 a4 r8 e' r8 d8 r8 
  a2.
  
  r4. fis'2. % F# omit 3 (fis cis' fis)
  
  e8 fis e % b' ??? gis'???? ("E")
  
  r8 e8~ e4 r4.
     fis2. e8 fis e4~ e4.
  
  r4 
  
  d2 
  e2 % but sharper (+13 cents)
  fis4 % maybe add cis'
  
  d8~ d4
  
  d' cis b8 r8
  
  r8
  d,2
  e2 % as above
  
  % naff key change
  
  fis2~ fis8~ fis4. e2.~ e2.
  d8 d d cis4
  cis8 cis a4~ a2 r2 % add cis to e maybe
  \bar "|."
}

theme = {
  cis8 d e cis r8 a r8
  
  d4 cis8 b a gis a b gis!
}


trackA = \relative c' {
  \clef tenor
  % \key a \major
  \set Staff.instrumentName = "Tbn. 1"
  \setup
  
  \theme
  
  cis d e cis r8 a r8
  
  \clef "tenor^8"
  fis'4 e8 fis gis a b e, d
  \clef "tenor"
  
  \theme
  
  cis4 a8 e4 d cis2.
  
  r4. cis'4. a'8 r8 gis 
  
  r8 cis,2.
  
  r4. cis4. a'8 r8 gis 
  
  r8 e2.
  
  e,8 f fis4 d'8 cis d4
  
  cis8 b b4 a8 a4 e'2~ e8
  
  e,8 f fis4 d'8 cis d4
  
  cis8 b
  
  % rather naff key change
  % \key b \major ? 
  % cis + bis
  fis'2 e2 cis4. e4.~ e2.
  
  d8 d d e4 cis8 cis a'4~ a2 r2 \bar "|."
   
}

\book {
  \header {
    title = "Power Doors"
    subsubtitle = \markup \center-column {
      "Operation by Driver Only"
      "General Appendix Instructions — 7th October 1989"
      " "
    }
    composer = "British Railways Board, arr. eta"
    tagline = "git.eta.st/eta/honking"
  }
\score {
  <<
   \new GrandStaff <<
    \new Staff \trackA
    \new Staff \bassline
    \new DrumStaff \percussion
   >>
  >>
  \layout {}
  \midi {}
}
}
