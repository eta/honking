\version "2.24.3"

setup = {
  \key d \major
  \time 4/4
  \tempo 4 = 182
  \clef tenor
  \set Staff.midiInstrument = #"trombone"
  \transposition c
}

trackA = \relative {
  \setup
  % verse 1
  r8 d' r d e4 fis4
  b a fis e8 d
  
  r8 b r b cis4 d
  fis e fis e
  
  r8 d r d e4 fis4
  b a fis e8 d
  
  r8 b r b cis4 d
  fis e8 e~ 4 r
  
  r8 d r d e4 fis4
  b4 a8  a8~ 8 r e4
  
  % bridge bit
  dis1 r1 r r 
  
  % verse 2
  \repeat volta 2 {
    d4 d cis d
    e d cis b
  
    r8 b r a g4 a
    g fis8 fis~ 4 r
  
    d'4 d cis d
    r8 e r d cis4 d
  
    r8 e r d cis4 d8 e
    e4 cis8 d8~ 8 r r4

    \alternative {
      \volta 1 {
        d4 d cis d
        e d cis4 d   
            
        r8 e r d cis4 d8 e
        e4 cis8 d8~ 8 r r4
      }
      \volta 2 {
        d4 d cis d
        r8 e r d cis4 d
               
        r8 e r d cis4 d8 e
        e4 cis8 d8~ 8 r r4
      }
    }
  }
  
  b4 b cis d
  b fis b fis
  
  a a cis e 
  a, e a e
  
  b' d cis b
  a cis e g
  
  fis1
  
  \bar "|."
}

\book {
  \header {
    title = "Rhythm Rally"
  }
  \score {
    <<
     \new GrandStaff <<
      \new Staff \trackA
     >>
    >>
    \layout {}
  }
}

\score {
  \unfoldRepeats \trackA
  \midi {}
}